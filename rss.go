package main

import (
  "fmt"
  "time"
)

type Item struct {
  Title string
  Author string
  Link string
  GUID string
  Timestamp time.Time
  Description string
}

func FormatHead() string {
  return fmt.Sprintf("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
      "<rss version=\"2.0\"><channel><title>%s</title>" +
      "<description>%s</description><link>%s</link>" +
      "<language>%s</language><copyright>%s</copyright>" +
      "<lastBuildDate>%s</lastBuildDate><generator>Alderman</generator>",
      ActiveConfig.Title, ActiveConfig.Description, ActiveConfig.RSSLink,
      ActiveConfig.Language, ActiveConfig.Copyright,
      time.Now().Format(ActiveConfig.RSSPubDateFormat))
}

func FormatImage() string {
  if !ActiveConfig.IncludeImage {
    return ""
  }
  return fmt.Sprintf("<image><url>%s</url><title>%s</title>" +
      "<link>%s</link><description>%s</description>" +
      "<width>%d</width><height>%d</height></image>",
      ActiveConfig.Image.URL, ActiveConfig.Image.Title, ActiveConfig.Image.Link,
      ActiveConfig.Image.Description, ActiveConfig.Image.Width, ActiveConfig.Image.Height)
}

func FormatItem(item Item) string {
  return fmt.Sprintf("<item><title>%s</title><author>%s</author><link>%s</link>" +
      "<guid>%s</guid><pubdate>%s</pubdate><description>%s</description></item>",
      item.Title, item.Author, item.Link, item.GUID,
      item.Timestamp.Format(ActiveConfig.RSSPubDateFormat), item.Description)
}

func FormatTail() string {
  return "</channel></rss>"
}
