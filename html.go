package main

import (
  "bufio"
  "golang.org/x/net/html"
  "time"
  "log"
  "os"
  "path"
)

func ParseHTML(fpath string) Item {
  url := ActiveConfig.BaseURL + fpath
  f, ferr := os.Open(path.Join(ActiveConfig.Directory, fpath))
  if ferr != nil {
    log.Fatal(ferr)
  }
  defer f.Close()
  p, perr := html.Parse(bufio.NewReader(f))
  if perr != nil {
    log.Fatal(perr)
  }
  var title, author string
  var pubdate time.Time
  var handleNode func(*html.Node)
  handleNode = func(n *html.Node) {
    if n.Type == html.ElementNode {
      for _, a := range n.Attr {
        if a.Key == "data-alderman" {
          if a.Val == "title" {
            if n.FirstChild.Type == html.TextNode {
              title = n.FirstChild.Data
              return
            } else {
              log.Fatal("Title must only contain text")
            }
          } else if a.Val == "author" {
            if n.FirstChild.Type == html.TextNode {
              author = n.FirstChild.Data
              return
            } else {
              log.Fatal("Author must only contain text")
            }
          } else if a.Val == "pubdate" {
            // TODO: this can almost certainly be optimized
            for _, a2 := range n.Attr {
              if a2.Key == "datetime" {
                time, terr := time.Parse(ActiveConfig.InputPubDateFormat, a2.Val)
                if terr != nil {
                  log.Fatal(terr)
                }
                pubdate = time
                return
              }
            }
            log.Fatal("Pubdate must contain a datetime attribute")
          }
        }
      }
    }
    for c := n.FirstChild; c != nil; c = c.NextSibling {
      handleNode(c)
    }
  }
  handleNode(p)
  return Item{
    Title: title,
    Author: author,
    Link: url,
    GUID: url,
    Description: "test",
    Timestamp: pubdate,
  }
}
