package main

type ImageConfig struct {
  Title string `json:"title"`
  Description string `json:"description"`
  URL string `json:"url"`
  Link string `json:"url"`
  Width uint8 `json:"width"`
  Height uint8 `json:"width"`
}

type Config struct {
  Directory string `json:"directory"`
  Title string `json:"title"`
  Description string `json:"description"`
  RSSLink string `json:"link"`
  BaseURL string `json:"baseURL"`
  Language string `json:"language"`
  Copyright string `json:"copyright"`
  IncludeImage bool `json:"includeImage"`
  Image ImageConfig `json:"image"`
  InputPubDateFormat string `json:"inputPubDateFormat"`
  RSSPubDateFormat string `json:"rssPubDateFormat"`
}

var defaultConfig = Config {
  Directory: "test",
  Title: "Example RSS Feed",
  Description: "An example RSS feed",
  RSSLink: "https://example.com/rss.xml",
  BaseURL: "https://example.com/",
  Language: "en-us",
  Copyright: "All rights reserved.",
  IncludeImage: true,
  Image: ImageConfig {
    Title: "Example RSS Feed",
    Description: "Example Logo",
    URL: "https://example.com/logo-64.png",
    Link: "https://example.com/rss.xml",
    Width: 64,
    Height: 64,
  },
  InputPubDateFormat: "2006-1-02 15:04:05-0700",
  RSSPubDateFormat: "Mon, 02 Jan 2006 15:04:05 -0700",
}

var ActiveConfig = defaultConfig
