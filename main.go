package main

import (
  "fmt"
  "log"
  "os"
  "path/filepath"
)

func main() {
  fmt.Print(FormatHead())
  fmt.Print(FormatImage())
  var items []Item
  err := filepath.Walk(ActiveConfig.Directory,
    func(path string, info os.FileInfo, err error) error {
      if err != nil {
        log.Fatal(err)
      }
      if info.IsDir() {
        return nil
      }
      rel, _ := filepath.Rel(ActiveConfig.Directory, path)
      items = append(items, ParseHTML(rel))
      return nil
  })
  if err != nil {
    log.Fatal(err)
  }
  for _, item := range items {
    fmt.Print(FormatItem(item))
  }
  fmt.Println(FormatTail())
}
